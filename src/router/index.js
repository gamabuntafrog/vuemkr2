import { createRouter, createWebHistory } from 'vue-router'
import Main from '../components/Main.vue'
import About from '../components/About.vue'
import CreateBook from '../components/CreateBook.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: Main
    },
    {
      path: '/createBook',
      name: 'createBook',
      component: CreateBook
    },
    {
      path: '/about',
      name: 'about',
      component: About
    }
  ]
})

export default router
